# End-to-End CI/CD Pipeline for Node.js Web Application using AWS CI/CD Services 🚀

Build a CI/CD pipeline for a Node.js web application using AWS CI/CD services (**CodeBuild, CodeDeploy, CodePipeline**).

## Architecture Description 📝

The architecture of the CI/CD pipeline is as follows:
Users start by pushing a new commit to the GitLab repository. This triggers the pipeline, which consists of three stages: **Source, Build, and Deploy**. The Source stage pulls the source code from the GitHub repository. The Build stage uses AWS CodeBuild to Test the Node.js web application. The Deploy stage uses AWS CodeDeploy to deploy the Node.js web application to an Elaastic Beanstalk environment.
**Note:** The build stage in this project is used to test the Node.js web application. In a real-world scenario, you would also include the build process in this stage.

## Architecture Diagram 📌
![aws-cicd-with-eb](https://github.com/mathesh-me/aws-cicd-devops-web-app/assets/144098846/f430491c-f767-4e2e-a7f6-2ba9f7e20ca5)

## Prerequisites 📋

- An AWS account with the necessary permissions to create the required resources.
- GitHub account

## Steps 📝

#### Step 1: Commit Application Configuration Files to GitHub Repository

#### Step-2: Create an Instance role Elastic Beanstalk
![iam](create_role.jpeg)

![role_detail](role_detail.jpg)

![role](role.jpg)


##### Configure Elastic Beanstalk Environment

### In this step, create an Elastic Beanstalk application and environment to deploy the Node.js web application.

#### Navigate to Elastic Beanstalk Console

![beanstalk](beanstalk.jpg)

![config_beans](config_beans.jpg)

![platform](platform.jpg)

![preset](preset.jpg)

![submit](submit.jpg)

![launch](launch_beans.jpg)
##### Elastic Beanstalk is launching

![congrats](congrats.jpg)


### Step-4: Create a Pipeline to Deploy Node.js Application
#### Use AWS CodePipeline to create a pipeline that automates the deployment process.

#### 1. Navigate to CodePipeline Console
![cp](cp.jpg)

![pipe-setting](pipe-setting.jpg)
##### Choose pipeline settings


![add-source](add-source.jpg)

![trigger](trigger.jpg)
##### Enable trigger

![filetr](filter-type.jpg)
![build](build.jpg)
##### Select Codepipeline as build provider

![create-build](create-build.jpg)
![buid](build-spec.jpg)

![success-created](success-created.jpg)

![add-deploy](add-deploy.jpg)

![create-pipe](create-pipe.jpg)
##### Create pipeline

![pipe-created](pipe-created.jpg)


![Failed](Failed.jpg)
##### The pipeline build failed because Input artifact option is set by default as BuildArtifact. Since we're not building any Binaries or Docker Images, BuildArtifacts Option is not required. Instead we will change the Input artifact option to SourceArtifact.

![edit-deploy](edit-deploy.jpg)

![save-deploy](save-deploy.jpg)
![save-changes](save-changes.jpg)
#### Save pipeline changes

![pipe-success1](pipe-success1.jpg)
![pipe-success2](pipe-success2.jpg)

### Step-5: View the Application
##### We can view the web application by accessing Elastic Beanstalk environment URL on our web browser
![web-app](web-app.jpg)




